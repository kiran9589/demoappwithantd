import { Plugin } from '/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/node_modules/@umijs/runtime';

const plugin = new Plugin({
  validKeys: ['patchRoutes','rootContainer','render','onRouteChange','dva','getInitialState','locale','locale','request',],
});
plugin.register({
  apply: require('/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/node_modules/umi-plugin-antd-icon-config/lib/app.js'),
  path: '/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/node_modules/umi-plugin-antd-icon-config/lib/app.js',
});
plugin.register({
  apply: require('/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/.umi/plugin-dva/runtime.tsx'),
  path: '/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/.umi/plugin-dva/runtime.tsx',
});
plugin.register({
  apply: require('../plugin-initial-state/runtime'),
  path: '../plugin-initial-state/runtime',
});
plugin.register({
  apply: require('/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/.umi/plugin-locale/runtime.tsx'),
  path: '/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/.umi/plugin-locale/runtime.tsx',
});
plugin.register({
  apply: require('../plugin-model/runtime'),
  path: '../plugin-model/runtime',
});

export { plugin };
