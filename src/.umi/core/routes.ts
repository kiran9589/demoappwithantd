import { ApplyPluginsType, dynamic } from '/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/node_modules/@umijs/runtime';
import { plugin } from './plugin';

const routes = [
  {
    "path": "/",
    "name": "Login",
    "icon": "login",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__user__login' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/user/login'), loading: require('@/components/PageLoading/index').default}),
    "exact": true
  },
  {
    "path": "/",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__SecurityLayout' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/layouts/SecurityLayout'), loading: require('@/components/PageLoading/index').default}),
    "routes": [
      {
        "path": "/",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__BasicLayout' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/layouts/BasicLayout'), loading: require('@/components/PageLoading/index').default}),
        "authority": [
          "admin",
          "user"
        ],
        "routes": [
          {
            "path": "/dashboard",
            "redirect": "/welcome",
            "exact": true
          },
          {
            "path": "/welcome",
            "name": "welcome",
            "icon": "smile",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__Welcome' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/Welcome'), loading: require('@/components/PageLoading/index').default}),
            "exact": true
          },
          {
            "path": "/welcome/NewWizard",
            "name": "",
            "icon": "smile",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__dashboard__NewWizard' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/dashboard/NewWizard'), loading: require('@/components/PageLoading/index').default}),
            "exact": true
          },
          {
            "path": "/admin",
            "name": "admin",
            "icon": "crown",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__Admin' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/Admin'), loading: require('@/components/PageLoading/index').default}),
            "authority": [
              "admin"
            ],
            "routes": [
              {
                "path": "/admin/sub-page",
                "name": "sub-page",
                "icon": "smile",
                "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__Welcome' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/Welcome'), loading: require('@/components/PageLoading/index').default}),
                "authority": [
                  "admin"
                ],
                "exact": true
              }
            ]
          },
          {
            "name": "list.table-list",
            "icon": "table",
            "path": "/list",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__ListTableList' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/ListTableList'), loading: require('@/components/PageLoading/index').default}),
            "exact": true
          },
          {
            "name": "Orders",
            "icon": "table",
            "path": "/orderManagement",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__orderManagement__OrderManagement' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/orderManagement/OrderManagement'), loading: require('@/components/PageLoading/index').default}),
            "exact": true
          },
          {
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/404'), loading: require('@/components/PageLoading/index').default}),
            "exact": true
          }
        ]
      },
      {
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/404'), loading: require('@/components/PageLoading/index').default}),
        "exact": true
      }
    ]
  },
  {
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/home/itl-946/Documents/Workspace/learning/TAviReact/demoappwithantd/src/pages/404'), loading: require('@/components/PageLoading/index').default}),
    "exact": true
  }
];

// allow user to extend routes
plugin.applyPlugins({
  key: 'patchRoutes',
  type: ApplyPluginsType.event,
  args: { routes },
});

export { routes };
