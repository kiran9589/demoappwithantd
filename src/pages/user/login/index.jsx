import { AlipayCircleOutlined, TaobaoCircleOutlined, WeiboCircleOutlined } from '@ant-design/icons';
import { Alert, Checkbox } from 'antd';
import React, { useState } from 'react';
import { Link, connect } from 'umi';
import LoginFrom from './components/Login';
import styles from './style.less';
import logo from '../../../assets/d1.jpg';
// import router from 'umi/router';

const { Tab, UserName, Password, Mobile, Captcha, Submit } = LoginFrom;

const LoginMessage = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

const Login = props => {
  const { userLogin = {}, submitting } = props;
  const { status, type: loginType } = userLogin;
  const [autoLogin, setAutoLogin] = useState(true);
  const [type, setType] = useState('account');

  const handleSubmit = values => {
    const { dispatch } = props;
    // dispatch({
    //   type: 'login/login',
    //   payload: { ...values, type },
    // });
    // debugger;
    // router.push("/welcome");
    
  };

  return (
    <div style={{"height":"100%","backgroundImage": "url("+logo+")"}}>
        <div className={styles.main}>
        <LoginFrom activeKey={type} onTabChange={setType} onSubmit={handleSubmit}>
        {status === 'error' && loginType === 'account' && !submitting && (
              <LoginMessage content="Invalid user credentials" />
            )}

            <UserName
              name="userName"
              placeholder="Please enter admin or user"
              rules={[
                {
                  required: true,
                  message: 'Please enter username!',
                },
              ]}
            />
            <Password
              name="password"
              placeholder="Please enter password"
              rules={[
                {
                  required: true,
                  message: 'Please enter password!',
                },
              ]}
            />

          
          <div>
            <Checkbox checked={autoLogin} onChange={e => setAutoLogin(e.target.checked)}>
              Remember Me
            </Checkbox>
          </div>
          <Submit loading={submitting}>
          <Link to="/welcome">
              Login
            </Link>
          </Submit>
          
          <div className={styles.other}>
            {/* <AlipayCircleOutlined className={styles.icon} />
            <TaobaoCircleOutlined className={styles.icon} />
            <WeiboCircleOutlined className={styles.icon} /> */}
            <a
              style={{
                float: 'left',
              }}
            >
              Forget Password
            </a>
            <Link className={styles.register} to="/user/register">
              Register User
            </Link>
          </div>
        </LoginFrom>
      </div>
    </div>
    
  );
};

export default connect(({ login, loading }) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))(Login);
