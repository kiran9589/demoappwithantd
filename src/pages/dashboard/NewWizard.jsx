import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Steps, Button, message,  Card } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import DBTableConfiguration  from  './DBTableConfiguration';
import Second from "./Second";

const { Step } = Steps;
const steps = [
  {
    title: 'First',
    content: <DBTableConfiguration isDatasetCall={true} />
  },
  {
    title: 'Second',
    content: <Second />
  }
];
// {steps[current].content}

class NewWizard extends React.Component {
  state = { visible: false, disableButton: true };
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
    };
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;

    return (
      <div>
        <Card>
          <Steps current={current}>
            {steps.map(item => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div className="steps-content">
              {steps[current].content}
          </div>
          <div className="steps-action">
            {current < steps.length - 1 && (
              <Button type="primary" onClick={() => this.next()}>
                Next
              </Button>
            )}
            {current === steps.length - 1 && (
              <Button type="primary" onClick={() => this.props.history.push('../welcome')}>
                Finish
              </Button>
            )}
            {current > 0 && (
              <Button style={{ margin: 8 }} onClick={() => this.prev()}>
                Previous
              </Button>
            )}
          </div>
        </Card>
      </div>
    );
  }
}

export default withRouter(NewWizard);
