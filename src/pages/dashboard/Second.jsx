import React, { Component } from "react";
import { Radio, Checkbox, Row, Col } from "antd";

function onChange(checkedValues) {
  console.log('checked = ', checkedValues);
}


class Second extends Component {
  render() {
    return (
      <div>
        <Row></Row>

        <Row>
          <Radio.Group style={{ margin: '0px 0px 20px 30px' }} name="radiogroup" defaultValue={1}>
            <Radio value={1}>A</Radio>
            <Radio value={2}>B</Radio>
            <Radio value={3}>C</Radio>
            <Radio value={4}>D</Radio>
          </Radio.Group>
        </Row>
        <Row>
            <Checkbox.Group style={{ margin: '0px 0px 0px 20px' }} onChange={onChange}>
              <Row>
                <Col span={8}>
                  <Checkbox value="A">A</Checkbox>
                </Col>
                <Col span={8}>
                  <Checkbox value="B">B</Checkbox>
                </Col>
                <Col span={8}>
                  <Checkbox value="C">C</Checkbox>
                </Col>
                <Col span={8}>
                  <Checkbox value="D">D</Checkbox>
                </Col>
                <Col span={8}>
                  <Checkbox value="E">E</Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>
        </Row>
      </div>
    );
  }
}

export default Second;
