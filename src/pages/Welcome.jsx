import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Typography, Alert, Row, Button } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './Welcome.less';
import { PlusOutlined } from '@ant-design/icons';

class Welcome extends React.Component {
  state = { visible: false, disableButton: true };

  showNewWizard = () => {
    let path = `welcome/NewWizard`;
    this.props.history.push(path);
  };

  render() {
    return (
      <div>
        <Row style={{width: '100%'}}>
          <div style={{ marginBottom:'20px',textAlign:'right' }}>
              <Button
                type="primary"
                icon={<PlusOutlined />}
                onClick={this.showNewWizard}
              >
                New Wizard
              </Button>
          </div>
        </Row>

        <Card>
          Test
        </Card>
      
    </div>
    );
  }
}

export default withRouter(Welcome);
