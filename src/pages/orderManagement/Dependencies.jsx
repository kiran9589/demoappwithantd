import React from 'react';
// import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Row, Col, Collapse, Menu, Dropdown   ,Button, Table } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './style.less';
const { Panel } = Collapse;
const { Meta } = Card;
import { DownOutlined, SearchOutlined } from '@ant-design/icons';
const menu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
          1st menu item
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
          2nd menu item
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
          3rd menu item
        </a>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: 'DEPENDENCY',
      dataIndex: 'dependency',
      sorter: {
        compare: (a, b) => a.dependency - b.dependency,
        multiple: 1,
      },
    },
    {
        title: 'VERSION',
        dataIndex: 'version',
    },
    {
        title: 'LATEST VERSION',
        dataIndex: 'latest_version',
    },
    {
        title: 'LAST PUBLISH',
        dataIndex: 'last_publish',
        sorter: {
          compare: (a, b) => a.last_publish - b.last_publish,
          multiple: 1,
        },
    },
    {
        title: 'VULNERABILITIES',
        dataIndex: 'vulnerabilities',
        sorter: {
          compare: (a, b) => a.vulnerabilities - b.vulnerabilities,
          multiple: 1,
        },
    },
    {
        title: 'LICENSE',
        dataIndex: 'license',
    },{
        title: 'Projects',
        dataIndex: 'projects',
    },{
      title: 'DEPENDENCIES WITH ISSUES',
      dataIndex: 'dependency_issue',
      sorter: {
        compare: (a, b) => a.dependency_issue - b.dependency_issue,
        multiple: 1,
      },
    }];
  
  const data = [
    {
        key: '1',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '2',
        dependency: 'John Brown',
        version: '3.2.11',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '3',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '4',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '5',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '6',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    },{
        key: '7',
        dependency: 'John Brown',
        version: '4.2.11.1',
        // latest_version: 60,
        // last_publish: 70,
        vulnerabilities: "0 H | 0 M | 0 L",
        // license: 20,
        projects: "1 project",
        dependency_issue: "-"
    }];

function onChange(pagination, filters, sorter, extra) {
console.log('params', pagination, filters, sorter, extra);
}
class Dependencies extends React.Component {
  state = { visible: false, disableButton: true };
  
  render() {
    return (
      <div className={styles.rowcls}>
         <Row gutter={2}>
            <Col span={16}>
                    <div style={{float:"left", marginRight:'10px'}}>
                        <Dropdown overlay={menu} placement="bottomLeft">
                            <Button> <SearchOutlined /> Dependencies  <DownOutlined /></Button>
                        </Dropdown>    
                    </div>
                    
                    <div  style={{float:"left", marginRight:'10px'}}>
                        <Dropdown overlay={menu} placement="bottomLeft">
                            <Button> Dependency Filter <DownOutlined /></Button>
                        </Dropdown>    
                    </div>
                    
                    <div  style={{float:"left", marginRight:'10px'}}>
                        <Dropdown overlay={menu} placement="bottomLeft">
                            <Button> 0 hidden fields <DownOutlined /></Button>
                        </Dropdown>    
                    </div>
                    
            </Col>
            <Col span={8} style={{textAlign:'right'}}>
                <Button type="primary">Export CSV</Button>
            </Col>
        </Row>   
        <Row style={{width:'100%', marginTop:'30px'}}>
            <Table columns={columns} dataSource={data} onChange={onChange} style={{width:'100%'}}/>
        </Row>
       </div>
    );
  }
}

export default withRouter(Dependencies);
