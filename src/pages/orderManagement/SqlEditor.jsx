import React from 'react';
import { Card, Row, Col, Tree, Input, Table, Descriptions,Button, Avatar } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './style.less';
import { CaretRightOutlined, FireOutlined, ClearOutlined, BellOutlined, CopyOutlined } from '@ant-design/icons';

const { TreeNode } = Tree;

class SqlEditor extends React.Component {
  state = { visible: false, disableButton: true };
  
  render() {
    const treeData = [
        {
          title: 'Scripting',
          key: 'scripiting',
          children: [
            {
              title: 'Scripts',
              key: 'scripts',
              children: [
                {
                  title: 'Active Rules',
                  key: 'activerules'
                },
                {
                  title: 'Authentication',
                  key: 'authentication',
                },
                {
                  title: 'Extender',
                  key: 'extender',
                  children: [
                    {
                      title: 'Extender 1',
                      key: 'extender1'
                    },
                    {
                        title: 'Extender 2',
                        key: 'extender 2'
                      },
                    ]
                },
                {
                  title: 'Fuzzer HTTP Processor',
                  key: 'httpProcessor',
                },
                {
                  title: 'Fuzzer WebSocket Processor',
                  key: 'websocketProcessor',
                },
                {
                  title: 'HTTP Sender',
                  key: 'httpSender',
                },
                {
                  title: 'Passive Rules',
                  key: 'passiveRules',
                },
                {
                  title: 'Payload Calculator',
                  key: 'payloadCalculator',
                },
                {
                  title: 'Payload Processor',
                  key: 'payloadProcessor',
                },
                {
                  title: 'Proxy',
                  key: 'proxy',
                },
                {
                  title: 'Script Input Vector',
                  key: 'scriptInputVector',
                },
                {
                  title: 'Stand Alone',
                  key: 'standAlone',
                },
                {
                  title: 'Targeted',
                  key: 'targeted',
                },
                {
                  title: 'WebSocket Sender',
                  key: 'websocketSender',
                },
              ],
            },
            {
              title: 'Templates',
              key: 'templates',
              children: [{
                title: 'Active Rules 1',
                key: 'activerules1'
              },
              {
                title: 'Authentication 1',
                key: 'authentication1',
              }],
            },
          ],
        },
      ];
      const columns = [
        {
          title: 'Id',
          dataIndex: 'id',
          key: 'id'
        },
        {
          title: 'Req. Timestamp',
          dataIndex: 'reqtimeStamp',
          key: 'reqtimeStamp',
        },
        {
            title: 'Resp. Timestamp',
            dataIndex: 'resptimeStamp',
            key: 'resptimeStamp',
        },
        {
          title: 'Method',
          dataIndex: 'method',
          key: 'method',
        },
        {
            title: 'URL',
            dataIndex: 'url',
            key: 'url',
        },
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
        },
        {
            title: 'Reason',
            dataIndex: 'reason',
            key: 'reason',
        },
        {
            title: 'RTT',
            dataIndex: 'rtt',
            key: 'rtt',
        },
        {
            title: 'Size Resp Header',
            dataIndex: 'sizeRespHeader',
            key: 'sizeRespHeader',
        },
        {
            title: 'Size Resp Body',
            dataIndex: 'sizeRespBody',
            key: 'sizeRespBody',
        },
      ];
    return (
      <div className={styles.siteCardWrapper}>
        <Row gutter={2}>
            <Col span={8}>
                <Card style={{ width: 310, minHeight:400 }}>
                    <Tree treeData={treeData} defaultExpandedKeys={['scripiting', 'scripts']} defaultSelectedKeys={['authentication']}/>
                </Card>
            </Col>
            <Col span={16}>
                <Row>
                    <Row>
                    <Button type="ghost" icon={<CaretRightOutlined />}>
                        Run 
                    </Button>
                    </Row>
                    <Input.TextArea style={{height:220, marginBottom: 10}} placeholder="To get started click the 'New Script...' button in the Scripts Tab on Left Hand side."/>
                </Row>
                <Row>
                    <Button type="ghost" icon={<ClearOutlined />} />
                    <Button type="ghost" icon={<BellOutlined />} style={{marginLeft: 5}}/>
                    <Button type="ghost" icon={<CopyOutlined />}  style={{marginLeft: 5}}/>

                    <Input.TextArea style={{height:220}} placeholder="Authentication scripts run when u an authentication needed. The Scripts must be properly configured in the session properties -> Authentication panel with a 'Scripts - based Authentication Method'"/>
                </Row>
            </Col>
         </Row>
         <Row>
            <Button type="ghost" icon={<FireOutlined />} style={{marginTop:10}}> New Scan </Button>
            <Table columns={columns} dataSource={[]} style={{width:'100%', marginTop:10}}/>
         </Row>
       </div>
    );
  }
}

export default withRouter(SqlEditor);
