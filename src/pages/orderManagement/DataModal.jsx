import React from 'react';
import { Modal, Row , Button,Form, Input,Divider, Card, Col, InputNumber,Select,Radio,List, Switch} from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './style.less';

const { Option } = Select;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 24 },
};

class DataModal extends React.Component {
  state = { modal1Visible: false,
    modal2Visible: false, value: 'existing'};

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }
  
    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }

    onChange = e => {
      console.log('radio checked', e.target.value);
      this.setState({
        value: e.target.value,
      });
    };
    
  
  render() {

    const Action = (
      <Switch
        checkedChildren='ON'
        unCheckedChildren='OFF'
        defaultChecked
      />
    );

    const uncheckedAction = (
      <Switch
        checkedChildren='ON'
        unCheckedChildren='OFF'
        defaultChecked={false}
      />
    );
  
  const data = [
      {
          title: 'Random Forest',
          actions: [Action]
      },
      {
          title: 'Gradient Tree Boosting',
          actions: [Action]
      },
      {
          title: 'Logistic Regression',
          actions: [uncheckedAction]
      },
      {
          title: 'XGBoost',
          actions: [Action]
      },
      {
          title: 'Decision Tree',
          actions: [uncheckedAction]
      },
      {
          title: 'Support Vector Machine',
          actions: [Action]
      },
      {
          title: 'Stochastic Gradient Decent',
          actions: [Action]
      },
      {
          title: 'KNN',
          actions: [uncheckedAction]
      },
      {
          title: 'Extra Random Trees',
          actions: [Action]
      },
      {
          title: 'Neural Network',
          actions: [Action]
      }
  ];
    return (
      <div className={styles.rowcls}> 
        <Row style={{width:'100%'}}>
          
          <Button type="primary" onClick={() => this.setModal1Visible(true)}>Model Endpoint</Button>
          
          
          <Button type="primary" style={{marginLeft:'20px'}}  onClick={() => this.setModal2Visible(true)}>Run Test</Button>
          
          
        </Row>

        <div>
            <Modal
              title="New ML Model Endpoint"
              visible={this.state.modal1Visible}
              onOk={() => this.setModal1Visible(false)}
              onCancel={() => this.setModal1Visible(false)}
            >

              <Form {...layout} name="nest-messages">
                <Form.Item name={['user', 'name']} label="Name">
                  <Input />
                </Form.Item>
                <Form.Item name={['user', 'website']} label="URL">
                  <Input />
                </Form.Item>
                <Form.Item name={['user', 'rate']} label="Sampling Rate">
                  <Divider orientation="left" style={{zIndex:'1000000'}}>% of traffic</Divider>
                  <Card style={{marginTop:'-29px'}}>
                      <Row>
                      <Col span={8}>
                      <InputNumber
                        defaultValue={10}
                        min={0}
                        max={100}
                        formatter={value => `${value}%`}
                      />
                      </Col>
                      <Col span={16}>
                      <Select defaultValue="hourly" style={{ width: 120 }}>
                        <Option value="hourly">Hourly</Option>
                        <Option value="daily">Daily</Option>
                        <Option value="weekly">Weekly</Option>
                      </Select>
                      </Col>
                      </Row> 
                  </Card>
                </Form.Item>
              </Form>
            </Modal>
          </div>
          
        <div>
            <Modal
              title="New Test Run"
              centered
              width={700}
              visible={this.state.modal2Visible}
              onOk={() => this.setModal2Visible(false)}
              onCancel={() => this.setModal2Visible(false)}
            >
              <Row>
                <Radio.Group onChange={this.onChange} value={this.state.value}>
                  <Radio value={'existing'}>Existing Models</Radio>
                  <Radio value={'custom'}>Custom Models</Radio>
                </Radio.Group>               
              </Row>
              <Divider />
              
              <Form {...layout} name="nest1-messages">
                <Form.Item name={['user', 'name']} label="Name">
                  <Input />
                </Form.Item>
                <Form.Item name={['user', 'website']} label="Data set Location">
                  <Input />
                </Form.Item>

                <Form.Item name={['user', 'website']} label="Select Models">
                {/* <Row> */}
                  <Col hidden = {this.state.value != 'existing'}>
                    <Card >
                        <Select defaultValue="holtwinter">
                            <Option value="holtwinter">Holt Winter</Option>
                            <Option value="arima">ARIMA</Option>
                            <Option value="seasonalArima">Seasonal ARIMA</Option>
                            <Option value="autoreg">AUTOREG</Option>
                            <Option value="seasonalESM">Seasonal Exponential Smoothing Method (ESM)</Option>
                            <Option value="ann">Artificial neural network (ANN)</Option>
                            <Option value="lstm">LSTM</Option>  
                        </Select>
                    </Card>
                  </Col>
                  <Col hidden = {this.state.value != 'custom'}>
                  <Card>
                                <List
                                      itemLayout="horizontal"
                                      size="small"
                                      dataSource={data}
                                      renderItem={item => (
                                      <List.Item actions={item.actions}>
                                          <List.Item.Meta                                                    
                                          title={item.title}                                                   
                                          />
                                      </List.Item>
                                      )}
                                  />
                  </Card>
                  </Col>
                {/* </Row> */}
                </Form.Item>
                <Form.Item name={['user', 'frequency']} label="Run Frequency">
                    <Select defaultValue="hourly" style={{ width: 120 }}>
                        <Option value="hourly">Hourly</Option>
                        <Option value="daily">Daily</Option>
                        <Option value="weekly">Weekly</Option>
                      </Select>
                </Form.Item>

              </Form>
            
          </Modal>
          </div>
      </div>
      
    );
  }
}

export default withRouter(DataModal);
