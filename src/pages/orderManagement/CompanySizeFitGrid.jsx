import React from 'react';
import { Card, Row, Col, Input, Table, Descriptions,Button, Avatar } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './style.less';

const { Search } = Input;

class CompanySizeFitGrid extends React.Component {
  state = { visible: false, disableButton: true };
  
  render() {
    function onChange(pagination, filters, sorter, extra) {
      console.log('params', pagination, filters, sorter, extra);
    }

    const columns = [
      {
        title: 'Date', dataIndex: 'date',
        sorter: {
          compare: (a, b) => a.date - b.date
        },
      },
      {
        title: 'Type', dataIndex: 'type',
        sorter: {
          compare: (a, b) => a.type - b.type
        },
      },
      {
        title: 'PROPHET_fit', dataIndex: 'profet_fit',
        sorter: {
          compare: (a, b) => a.profet_fit - b.profet_fit
        },
      },
      {
        title: 'PROPHET_lwr', dataIndex: 'profet_lwr',
        sorter: {
          compare: (a, b) => a.profet_lwr - b.profet_lwr
        },
      },
      {
        title: 'PROPHET_upr', dataIndex: 'profet_upr',
        sorter: {
          compare: (a, b) => a.profet_upr - b.profet_upr
        },
      },
      {
        title: 'HW_fit', dataIndex: 'hw_fit',
        sorter: {
          compare: (a, b) => a.hw_fit - b.hw_fit
        },
      },
      {
        title: 'HW_lwr', dataIndex: 'hw_lwr',
        sorter: {
          compare: (a, b) => a.hw_lwr - b.hw_lwr
        },
      },
      {
        title: 'HW_upr', dataIndex: 'hw_upr',
        sorter: {
          compare: (a, b) => a.hw_upr - b.hw_upr
        },
      },
      {
        title: 'pyro_fit', dataIndex: 'pyro_fit',
        sorter: {
          compare: (a, b) => a.pyro_fit - b.pyro_fit
        },
      },
      {
        title: 'pyro_upr', dataIndex: 'pyro_upr',
        sorter: {
          compare: (a, b) => a.pyro_upr - b.pyro_upr
        },
      },
      {
        title: 'nbeats_fit', dataIndex: 'nbeats_fit',
        sorter: {
          compare: (a, b) => a.nbeats_fit - b.nbeats_fit
        },
      },
      {
        title: 'nbeats_lwr', dataIndex: 'nbeats_lwr',
        sorter: {
          compare: (a, b) => a.nbeats_lwr - b.nbeats_lwr
        },
      },
      {
        title: 'nbeats_upr', dataIndex: 'nbeats_upr',
        sorter: {
          compare: (a, b) => a.nbeats_upr - b.nbeats_upr
        },
      },
      {
        title: 'lstm_fit', dataIndex: 'lstm_fit',
        sorter: {
          compare: (a, b) => a.lstm_fit - b.lstm_fit
        },
      },
      {
        title: 'lstm_lwr', dataIndex: 'lstm_lwr',
        sorter: {
          compare: (a, b) => a.lstm_lwr - b.lstm_lwr
        },
      },
    ];

    const data = [
      {key: '1', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '2', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '3', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '4', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '4', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '5', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '6', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '7', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '8', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '9', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '10', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '11', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189},
      {key: '12', date: '2021-06-01', type:'Predicted', 'profet_fit' : 825856, 'profet_lwr' :1838384, 'profet_upr':2852642, 'hw_fit': 1876958, 'hw_lwr': 460656, 'hw_upr': 3293261 , 'pyro_fit': 5894,'pyro_upr': 9780, 'nbeats_fit': 25, 'nbeats_lwr': 20, 'nbeats_upr': 30,  'lstm_fit': -106486,  'lstm_lwr': -85189}

    ];
      
    return (
      <div className={styles.rowcls}>
        <Row gutter={2}>
          <Descriptions>
            <Descriptions.Item label="Company">ACME</Descriptions.Item>
            <Descriptions.Item label="Account">bbb2e213233213hhg323fg2321h</Descriptions.Item>
            <Descriptions.Item label="Level">bd7c6f552424hjgh424jg42j</Descriptions.Item>
            <Descriptions.Item label="Version">bd7c6f552424hjgh424jg42j</Descriptions.Item>
          </Descriptions>
         </Row>
         <Row>
              <Col span={16}>  
                <Button type="ghost">CSV</Button>
                <Button type="ghost" style={{marginLeft: 5}}>Excel</Button>
                <Button type="ghost" style={{marginLeft: 5}}>Column Visibility</Button>
                
              </Col>
              <Col span={8} style={{textAlign:'right'}}>
              <Search
                placeholder="Input search text"
                onSearch={value => console.log(value)}
                style={{ width: 200 }}
              />
              </Col>
              
         </Row>
         <Row style={{marginTop:10}}>
            <Table columns={columns} dataSource={data} onChange={onChange} scroll={{x:1000}} style={{
              tableLayout:'fixed', width:'1020px'
            }}
            
            />
         </Row>
       </div>
    );
  }
}

export default withRouter(CompanySizeFitGrid);
