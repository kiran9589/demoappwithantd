import React from 'react';
// import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Tabs, Card, Statistic, Space,Typography, Alert, Row, Col, Button, Progress } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import styles from './style.less';
import {
  LineChart,
  AreaChart,
  PieChart,
  Pie,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Line,
  ComposedChart,
  Area,
  Bar,
} from 'recharts';
import { PlusOutlined } from '@ant-design/icons';
import Vulnerability from './Vulnerability'
import Dependencies from './Dependencies'
import SqlEditor from './SqlEditor'
import CompanySizeFitGrid from './CompanySizeFitGrid'


const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

function getTabContent(type){
    var content = null;  

    
    if(type === 'summary'){
      const data = [
        {
          name: 'Page A',
          uv: 0,
          amt: 150,
        },
        {
          name: 'Page B',
          uv: 0,
          amt: 150,
        },
        {
          name: 'Page C',
          uv: 0,
          amt: 150,
        },
        {
          name: 'Page D',
          uv: 0,
          amt: 150,
        },
        {
          name: 'Page E',
          uv: 120,
          amt: 150,
        },
        {
          name: 'Page F',
          uv: 0,
          amt: 150,
        },
        {
          name: 'Page G',
          uv: 0,
          amt: 150,
        },
      ];

      const rangeData = [
        {
          "day": "05-01",
          "approved": [
            -1,
            10
          ],
          "pending": [
            10,
            20
          ],
          "critical": [
            20,
            30
          ]
        },
        {
          "day": "05-02",
          "approved": [
            -6,
            5
          ],
          "pending": [
            5,
            14
          ],
          "critical": [
            14,
            30
          ]
        },
        {
          "day": "05-03",
          "approved": [
            -2,
            10
          ],
          "pending": [
            10,
            11
          ],
          "critical": [
            11,
            30
          ]
        },
        {
          "day": "05-04",
          "approved": [
            -9,
            8
          ],
          "pending": [
            8,
            11
          ],
          "critical": [
            11,
            22
          ]
        },
        {
          "day": "05-05",
          "approved": [
            -1,
            10
          ],
          "pending": [
            10,
            17
          ],
          "critical": [
            17,
            25
          ]
        },
        {
          "day": "05-06",
          "approved": [
            -1,
            5
          ],
          "pending": [
            5,
            18
          ],
          "critical": [
            18,
            26
          ]
        },
        {
          "day": "05-07",
          "approved": [
            -1,
            18
          ],
          "pending": [
            18,
            22
          ],
          "critical": [
            22,
            30
          ]
        },
        {
          "day": "05-08",
          "approved": [
            -9,
            9
          ],
          "pending": [
            9,
            20
          ],
          "critical": [
            20,
            30
          ]
        },
        {
          "day": "05-09",
          "approved": [
            -1,
            10
          ],
          "pending": [
            10,
            20
          ],
          "critical": [
            20,
            30
          ]
        }
      ]

      const data01 = [
        {
          "name": "Compliant",
          "value": 50
        },
        {
          "name": "Non Compliant",
          "value": 50
        }];
      
       content =   <div className={styles.siteCardWrapper}>
       <Row gutter={2}>
         <Col span={14}>
           <Card title="Vulnerabilities" bordered={false} headStyle={{fontWeight: 'bold'}}>
                <Row>
                  <Space size="middle">
                    <Statistic title="Total" value={86} valueStyle={{ color: 'grey' , fontSize:'30px', width: '90px' }} style={{ fontWeight: 'bold'}} />
                    <Statistic title="Critical" value={11} valueStyle={{ color: '#ff471a' , fontSize:'30px', width: '90px'}} style={{ fontWeight: 'bold'}}/>
                    <Statistic title="High" value={42}  valueStyle={{ color: '#ff9900' , fontSize:'30px', width: '90px'}} style={{ fontWeight: 'bold'}}/>
                    <Statistic title="Medium" value={33}  valueStyle={{ color: '#ffcc00' , fontSize:'30px', width: '90px'}} style={{ fontWeight: 'bold'}}/>
                    <Statistic title="Low" value={0}  valueStyle={{ color: '#1a8cff' , fontSize:'30px', width: '90px'}} style={{ fontWeight: 'bold'}}/>
                  </Space>
                </Row>
           </Card>
         </Col>
         <Col span={10}>
           <Card title="Legal Compliance" bordered={false}  headStyle={{fontWeight: 'bold'}}>
           <Row>
                  <Space size="middle">
                    <Statistic title="Total" value={86} valueStyle={{ color: 'grey' , fontSize:'30px', width: '80px' }} style={{ fontWeight: 'bold'}} />
                    <Statistic title="Denied" value={11} valueStyle={{ color: '#ff471a' , fontSize:'30px', width: '80px'}} style={{ fontWeight: 'bold'}}/>
                    <Statistic title="Pending" value={42}  valueStyle={{ color: '#ff9900' , fontSize:'30px', width: '80px'}} style={{ fontWeight: 'bold'}}/>
                    <Statistic title="Approved" value={33}  valueStyle={{ color: '#009933' , fontSize:'30px', width: '80px'}} style={{ fontWeight: 'bold'}}/>
                  </Space>
                </Row>
           </Card>
         </Col>
       </Row>
       <Row gutter={16} style={{marginTop: "20px"}}>
         <Col span={14}>
           <Card title="App Vulnerability Trend" bordered={false} headStyle={{fontWeight: 'bold'}}>
                <Row>
                    <LineChart
                      width={550}
                      height={390}
                      data={data}
                      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      <Line type="monotone" dataKey="uv" title="Timeline in weeks" stroke="#82ca9d" />
                    </LineChart>
                </Row>
           </Card>
         </Col>
         <Col span={10}>
           <Card title="Total Vulnerabilities" bordered={false}  headStyle={{fontWeight: 'bold'}}>
              <Row gutter={16}>
                  <Col span={12}>
                    <Card bordered={false}>
                        <Progress type="circle" percent={7.09} strokeColor="#e62e00" trailColor="#ffc2b3" strokeWidth="5" format={percent => `7.09% Critical`}/>
                        <div className={styles.progressBarContent}>Total : 3</div>
                    </Card>
                  </Col>
                  <Col span={12}>
                    <Card bordered={false}>
                        <Progress type="circle" percent={50} strokeColor="#ff9900" trailColor="#ffd699" strokeWidth="5" format={percent => '50% High'}/>
                        <div className={styles.progressBarContent}>Total : 10</div>
                    </Card>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Card bordered={false}>
                        <Progress type="circle" percent={42} strokeColor="#666633" trailColor="#d4d4aa" strokeWidth="5" format={percent => `42% Medium`}/>
                        <div className={styles.progressBarContent}>Total : 16</div>
                    </Card>
                  </Col>
                  <Col span={12}>
                    <Card bordered={false}>
                        <Progress type="circle" percent={0} strokeColor="#008ae6" trailColor="#b3e0ff" strokeWidth="5" format={percent => `0% Low`}/>
                        <div className={styles.progressBarContent}>Total : 0</div>
                    </Card>
                  </Col>
                </Row>
           </Card>
         </Col>
       </Row>
       <Row gutter={16} style={{marginTop: "20px"}}>
         <Col span={14}>
           <Card title="State of policies over time" bordered={false} headStyle={{fontWeight: 'bold'}}>
                <Row>
                <AreaChart
                    width={580}
                    height={350}
                    data={rangeData}
                    margin={{
                      top: 10, right: 10, bottom: 10, left: 0,
                    }}
                  >
                    <XAxis dataKey="day" />
                    <YAxis />
                    <Legend verticalAlign="top" layout="vertical" align="right" iconType="circle" />
                    <Area dataKey="critical" fill="#e62e00" stroke="#ffc2b3" />
                    <Area dataKey="pending" fill="#ff9900" stroke="#ffd699" />
                    <Area dataKey="approved" fill="#008ae6" stroke="#b3e0ff" />
                    <Tooltip />
                  </AreaChart>
                </Row>
           </Card>
         </Col>
         <Col span={10}>
           <Card title="Compliant Vs Non Compliant Licenses" bordered={false}  headStyle={{fontWeight: 'bold'}}>
                <Row>

                <PieChart width={350} height={250}>
                  <Legend verticalAlign="top" iconType="square" />
                  <Pie data={data01} dataKey="value" nameKey="name" cx="50%" cy="50%" endAngle={360} outerRadius={100} fill="#666633"/>
                  
                </PieChart>
                  
                </Row>
           </Card>
         </Col>
       </Row>
     </div>
    } else if(type === 'vulnerability'){
      content = <Vulnerability> </Vulnerability>
    } else if(type === 'dependencies'){
      content = <Dependencies> </Dependencies>
    } else if(type === 'licenses') {
      content = <SqlEditor> </SqlEditor>
    } else if(type === 'settings') {
      content = <CompanySizeFitGrid> </CompanySizeFitGrid>
    }

    return content;
}

class OrderManagement extends React.Component {
  state = { visible: false, disableButton: true };


  render() {
    return (
      <div>
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="Summary" key="1">
             { getTabContent('summary')}
          </TabPane>
          <TabPane tab="Vulnerability (86)" key="2">
            {/* <Vulnerability> </Vulnerability> */}
            { getTabContent('vulnerability')}
          </TabPane>
          <TabPane tab="Dependencies" key="3">
            { getTabContent('dependencies')}
          </TabPane>
          <TabPane tab="Licenses" key="4">
            { getTabContent('licenses')}
          </TabPane>
          <TabPane tab="Settings" key="5">
            { getTabContent('settings')}
          </TabPane>
        </Tabs>
    </div>
    );
  }
}

export default withRouter(OrderManagement);
